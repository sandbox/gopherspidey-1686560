<?php
/**
 * @file
 * gdlc_sitewide.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function gdlc_sitewide_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Address';
  $fe_block_boxes->format = 'html';
  $fe_block_boxes->machine_name = 'gdlc_address';
  $fe_block_boxes->body = '<p>8301 Aurora Avenue Urbandale, IA 50322 Phone: 515-276-1700 Fax: 515-276-5939 <a href="http://maps.google.com/maps?daddr=8301%20Aurora%20Avenue,Urbandale,IA%2050322" target="_blank"><img src="http://maps.googleapis.com/maps/api/staticmap?center=41.637072,-93.734175&amp;zoom=14&amp;size=200x200&amp;markers=color:0x99cc33%7Clabel:G%7C41.637072,-93.734175&amp;style=feature:poi%7Celement:all%7Cvisibility:off&amp;sensor=false" /></a></p>';

  $export['gdlc_address'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Slogan (html)';
  $fe_block_boxes->format = 'html';
  $fe_block_boxes->machine_name = 'gdlc_block_slogan';
  $fe_block_boxes->body = '<p>Bringing <b><i>Jesus Christ</i></b> to people<br />for the first time and a lifetime.</p>';

  $export['gdlc_block_slogan'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Worship Times';
  $fe_block_boxes->format = 'html';
  $fe_block_boxes->machine_name = 'gdlc_worship_times';
  $fe_block_boxes->body = '<p><strong>SATURDAY | Family Life Center</strong><br />Contemporary Worship at 5:30 pm</p><p><strong>SUNDAY | Sanctuary</strong></p><p>Traditional Worship at 8:00 am<br />Traditional Blend Worship at 9:30 am<br />Contemporary Worship at 11:00 am</p><p><strong>SUNDAY | Family Life Center</strong></p><p>Contemporary Worship at 10:00 am</p>';

  $export['gdlc_worship_times'] = $fe_block_boxes;

  return $export;
}
