<?php
/**
 * @file
 * gdlc_sitewide.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gdlc_sitewide_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fontyourface_features_default_font().
 */
function gdlc_sitewide_fontyourface_features_default_font() {
  return array(
    'Open Sans 300 (latin)' => array(
      'name' => 'Open Sans 300 (latin)',
      'enabled' => '1',
      'url' => 'http://www.google.com/webfonts/family?family=Open+Sans&subset=latin#300',
      'provider' => 'google_fonts_api',
      'css_selector' => '',
      'css_family' => 'Open Sans',
      'css_style' => 'normal',
      'css_weight' => '300',
      'foundry' => 'Steve Matteson',
      'foundry_url' => 'http://www.google.com/webfonts/designer?designer=Steve+Matteson',
      'license' => 'Apache License, version 2.0',
      'license_url' => 'http://www.apache.org/licenses/LICENSE-2.0.html',
      'metadata' => 'a:2:{s:4:"path";s:13:"Open+Sans:300";s:6:"subset";s:5:"latin";}',
      'css_fallbacks' => '',
      'designer' => '',
      'designer_url' => '',
    ),
    'Open Sans 400 (latin)' => array(
      'name' => 'Open Sans 400 (latin)',
      'enabled' => '1',
      'url' => 'http://www.google.com/webfonts/family?family=Open Sans&subset=latin#400',
      'provider' => 'google_fonts_api',
      'css_selector' => '<none>',
      'css_family' => 'Open Sans',
      'css_style' => 'normal',
      'css_weight' => '400',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:13:"Open Sans:400";s:6:"subset";s:5:"latin";}',
      'css_fallbacks' => '',
      'designer' => '',
      'designer_url' => '',
    ),
  );
}
